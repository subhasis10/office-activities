package com.enparadigm.com.activities.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.enparadigm.activities.beans.Feedback_submitted;
import com.enparadigm.activities.daos.Feedback_submittedDAO;

/**
 * Servlet implementation class FeedbackSubmittedServlet
 */
@WebServlet("/FeedbackSubmittedServlet")
public class FeedbackSubmissionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FeedbackSubmissionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String status=null;
		String name=request.getParameter("name");
		String feedback=request.getParameter("feed");
		int question1_id=Integer.parseInt(request.getParameter("question1_id"));
		int question2_id=Integer.parseInt(request.getParameter("question2_id"));
		int question3_id=Integer.parseInt(request.getParameter("question3_id"));
		int rating1=Integer.parseInt(request.getParameter("rating1"));
		int rating2=Integer.parseInt(request.getParameter("rating2"));
		int rating3=Integer.parseInt(request.getParameter("rating3"));
		int program_id =Integer.parseInt(request.getParameter("program_id"));
		Feedback_submittedDAO Feedback_submittedDAOobj = new Feedback_submittedDAO();
		Feedback_submitted feedback_submitted = new Feedback_submitted();
		feedback_submitted.setName(name);
		feedback_submitted.setFeedback(feedback);
		feedback_submitted.setQuestion1_id(question1_id);
		feedback_submitted.setQuestion2_id(question2_id);
		feedback_submitted.setQuestion3_id(question3_id);
		feedback_submitted.setRating1(rating1);
		feedback_submitted.setRating2(rating2);
		feedback_submitted.setRating3(rating3);
		feedback_submitted.setProgram_id(program_id);
//		boolean result=Feedback_submittedDAOobj.insertFeedback_submitted(name, feed, question1_id, question2_id, question3_id, rating1, rating2, rating3, program_id);
		
		boolean result = false;
		result = Feedback_submittedDAOobj.insertFeedback_submitted(feedback_submitted);
		if(result==true)
			status="Successful";
		else
			status="Failure";
		write(response,status);
		
	}
	private void write(HttpServletResponse response,String status) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(status));
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
