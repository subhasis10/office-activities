package com.enparadigm.com.activities.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.util.*;
import com.google.gson.*;
//import java.text.*;

import com.enparadigm.activities.beans.Admin;
import com.enparadigm.activities.daos.AdminDAO;


/**
 * Servlet implementation class AdminAuthenticationServlet
 */
@WebServlet("/AdminAuthenticationServlet")
public class AdminAuthenticationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminAuthenticationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		AdminDAO AdminDAOobj=new AdminDAO();
		ArrayList<Admin> adminlist=AdminDAOobj.fetchAdminList();
		Map<String,Object> mapobj=new HashMap<String,Object>();
		int iteration=0;
		String admin_name=request.getParameter("name");
		String admin_password=request.getParameter("password");
		
		try
		{
			while(iteration<adminlist.size())
			{
				
				if((adminlist.get(iteration).getAdmin_name()==admin_name)&&(adminlist.get(iteration).getAdmin_password()==admin_password))
				{
					break;
					
				}
				else
				iteration++;
				
			}
			String message=adminlist.get(iteration).getAdmin_name();
			mapobj.put("admin",message);
			write(response,mapobj);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void write(HttpServletResponse response,Map<String,Object> map) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(map));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
