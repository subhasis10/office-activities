package com.enparadigm.com.activities.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import com.enparadigm.activities.beans.Category;
import com.enparadigm.activities.beans.Suggestion;
import com.enparadigm.activities.daos.CategoryDAO;
import com.enparadigm.activities.daos.SuggestionDAO;
/**
 * Servlet implementation class SuggestionSubmissionServlet
 */
@WebServlet("/SuggestionSubmissionServlet")
public class SuggestionSubmissionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SuggestionSubmissionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String status=null;
		String name=request.getParameter("name");
		String suggestion=request.getParameter("suggestion");
		String category_name=request.getParameter("category_name");
		
		Category Categoryobj = new CategoryDAO().fetchCategoryByName(category_name);
		Suggestion objSuggestion = new Suggestion();
		int category_id = Categoryobj.getCategory_id();
		
		objSuggestion.setSuggester_name(name);
		objSuggestion.setSuggestion(suggestion);
		objSuggestion.setCategory_id(category_id);
		
		SuggestionDAO SuggestionDAOobj=new SuggestionDAO();
		boolean result = SuggestionDAOobj.insertSuggestion(objSuggestion);
		
		if(result==true)
			status="Successful";
		else
			status="Failure";
		write(response,status);
		
	}
	private void write(HttpServletResponse response,String status) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(status));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
