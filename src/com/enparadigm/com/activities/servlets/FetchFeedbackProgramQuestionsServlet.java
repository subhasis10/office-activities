package com.enparadigm.com.activities.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.util.ArrayList;
import com.google.gson.Gson;


import com.enparadigm.activities.beans.Feedback_question;
import com.enparadigm.activities.daos.FetchFeedbackProgramQuestionsDAO;

/**
 * Servlet implementation class FetchProgramQuestionsServlet
 */
@WebServlet("/FetchProgramQuestionsServlet")
public class FetchFeedbackProgramQuestionsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FetchFeedbackProgramQuestionsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
					
		try
		{
			String program_name=request.getParameter("program_name");
			FetchFeedbackProgramQuestionsDAO FetchFeedbackProgramQuestionsDAOobj=new FetchFeedbackProgramQuestionsDAO();
			ArrayList<Feedback_question> feedbackprogramquestionlist=FetchFeedbackProgramQuestionsDAOobj.fetchFeedback_programquestionList(program_name);
			ArrayList<Feedback_question> feedbackformquestionlist=new ArrayList<Feedback_question>();
			
			if(feedbackprogramquestionlist.size()<=3)
				feedbackformquestionlist=feedbackprogramquestionlist;
			else
			{
				feedbackformquestionlist.add(feedbackprogramquestionlist.get(0));
				feedbackformquestionlist.add(feedbackprogramquestionlist.get(1));
				feedbackformquestionlist.add(feedbackprogramquestionlist.get(2));
			}
				write(response,	feedbackformquestionlist);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

			
}
	private void write(HttpServletResponse response,ArrayList<Feedback_question> list) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(list));
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
