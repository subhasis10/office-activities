package com.enparadigm.com.activities.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.enparadigm.activities.beans.Category;
import com.enparadigm.activities.daos.CategoryDAO;

import java.util.ArrayList;
import com.google.gson.Gson;

/**
 * Servlet implementation class FetchCategoryListServlet
 */
@WebServlet("/FetchCategoryListServlet")
public class FetchCategoryListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FetchCategoryListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try
		{
			CategoryDAO CategoryDAOobj=new CategoryDAO();
			ArrayList<Category> categorylist=CategoryDAOobj.fetchCategoryList();
			
			write(response,	categorylist);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

			
}
	private void write(HttpServletResponse response,ArrayList<Category> list) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(list));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
