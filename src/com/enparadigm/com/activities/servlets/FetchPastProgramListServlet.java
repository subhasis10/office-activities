package com.enparadigm.com.activities.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import com.google.gson.Gson;

import com.enparadigm.activities.daos.Program_metaDAO;
import com.enparadigm.activities.beans.Program_meta;

/**
 * Servlet implementation class FetchPastProgramListServlet
 */
@WebServlet("/FetchPastProgramListServlet")
public class FetchPastProgramListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FetchPastProgramListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Program_metaDAO Program_metaDAOobj=new Program_metaDAO();
		ArrayList<Program_meta> programlist=Program_metaDAOobj.fetchProgram_metaList();
		ArrayList<Program_meta> pastprogramlist=new ArrayList<Program_meta>();
		int iteration=0;
		try
		{
			while(iteration<programlist.size())
			{
				String currentDate=request.getParameter("currentdate");
				//String program_name=programlist.get(iteration).getProgram_name();
				String program_date=programlist.get(iteration).getProgram_date().toString();
				//Date current_date=new SimpleDateFormat("yyyy-mm-dd").parse(currentDate);
				//if(programlist.get(iteration).getProgram_date().compareTo(current_date)>=0)
				if(program_date.compareTo(currentDate)<0)
				{
					
					pastprogramlist.add(programlist.get(iteration));
					
				}
				iteration++;
				
			}	
			write(response,pastprogramlist);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void write(HttpServletResponse response,ArrayList<Program_meta> list) throws IOException
	{
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(list));
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

}
