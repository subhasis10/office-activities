package com.enparadigm.com.activities.servlets;

import com.enparadigm.activities.beans.Program_meta;
import com.enparadigm.activities.daos.Program_metaDAO;

import java.io.IOException;
//import java.util.ArrayList;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson; 
import com.google.gson.GsonBuilder;  

@WebServlet("/GetDateServlet")
public class GetDateServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String program_id = request.getParameter("program_id").trim();
		Program_metaDAO objProgram_metaDAO = new Program_metaDAO();
		Program_meta objProgram_meta = new Program_meta();
		objProgram_meta = objProgram_metaDAO.fetchProgram_meta(Integer.parseInt(program_id));
		
		GsonBuilder builder = new GsonBuilder(); 
	    builder.setPrettyPrinting();
	    Gson gson = builder.create(); 
	    
	    String jsonString = gson.toJson(objProgram_meta); 
	    System.out.println(jsonString); 
		
		PrintWriter pw = response.getWriter();
		pw.print(jsonString);
    }
}
