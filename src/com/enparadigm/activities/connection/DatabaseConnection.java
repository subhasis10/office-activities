package com.enparadigm.activities.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DatabaseConnection {
	
	private static DatabaseConnection obj_DatabaseConnection = null;
	
	private DatabaseConnection()
	{
		
	}
	
	public static DatabaseConnection getInstance()
	{
		if(obj_DatabaseConnection == null)
	        obj_DatabaseConnection = new DatabaseConnection();
		
		return obj_DatabaseConnection;
	}
	
	public Connection getConnection()
	{
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver"); 
			String url = "jdbc:mysql://localhost:3306/activity?useSSL=false";
		    String user = "root";
		    String password = "";
			conn = DriverManager.getConnection(url, user, password);
		}
		catch (Exception e) {
            e.printStackTrace();
        }
		return conn;
	}
	
	public boolean releaseConnection(Connection connection) {
		closeConnection(connection);
		return true;
	}

	private boolean closeConnection(Connection connection) {

		try {
			connection.close();

		}
		catch (SQLException e) {

			e.printStackTrace();
		}
		return true;
	}

}
