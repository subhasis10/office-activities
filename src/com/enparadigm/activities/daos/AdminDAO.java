package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enparadigm.activities.beans.Admin;
import com.enparadigm.activities.connection.DatabaseConnection;


public class AdminDAO {
	
	public static void main(String args[])
	{
//		Admin admin = new Admin();
//		
//		int admin_id = 3;
//		String admin_name = "Vishal";
//		String admin_email = "vishal123@gmail.com";
//		String admin_password = "somepass2";
//		
//		admin.setAdmin_id(admin_id);
//		admin.setAdmin_name(admin_name);
//		admin.setAdmin_email(admin_email);
//		admin.setAdmin_password(admin_password);
//		
//		admin = fetchAdmin(admin_id);
//		System.out.println(admin.getAdmin_id());
//		System.out.println(admin.getAdmin_name());
//		System.out.println(admin.getAdmin_email());
//		System.out.println(admin.getAdmin_password());
//		
//		boolean status = false;
//		status = insertAdmin(admin);
//		status = deleteAdmin(admin_id);
//		status = updateAdmin(admin);
//		if(status == true)
//			System.out.println("Successful");
//		else
//			System.out.println("Unsuccessful");
		
//		ArrayList<Admin> adminList = fetchAdminList();
//		for(Admin admin1 : adminList)
//		{
//			System.out.println(admin1.getAdmin_id());
//			System.out.println(admin1.getAdmin_name());
//			System.out.println(admin1.getAdmin_email());
//			System.out.println(admin1.getAdmin_password());
//		}
		return;
	}	
	
	public ArrayList<Admin> fetchAdminList()
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Admin> adminList = new ArrayList<Admin>();
		try {
			String sql = "SELECT * FROM admin ORDER BY admin_id";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while(rs.next())
			{
				Admin admin = new Admin();
				admin.setAdmin_id(rs.getInt("admin_id"));
				admin.setAdmin_name(rs.getString("admin_name"));
				admin.setAdmin_email(rs.getString("admin_email"));
				admin.setAdmin_password(rs.getString("admin_password"));
				adminList.add(admin);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return adminList;
	}
	
	public Admin fetchAdmin(int admin_id) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		Admin admin = new Admin();
		try{
			String sql = "SELECT * FROM admin WHERE admin_id=?"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,admin_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				admin.setAdmin_id(rs.getInt("admin_id"));
				admin.setAdmin_name(rs.getString("admin_name"));
				admin.setAdmin_email(rs.getString("admin_email"));
				admin.setAdmin_password(rs.getString("admin_password"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally{
			objDatabaseConnection.releaseConnection(connection);
		}
		return admin;
	}
	
	public boolean insertAdmin(Admin admin)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "INSERT INTO admin(admin_name, admin_email, admin_password) VALUES (?,?,?)";
			ps = connection.prepareStatement(sql);
			ps.setString(1,admin.getAdmin_name());
			ps.setString(2,admin.getAdmin_email());
			ps.setString(3,admin.getAdmin_password());
			ps.execute();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean updateAdmin(Admin admin)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE admin SET admin_name=?, admin_email=?, admin_password=? WHERE admin_id=?";
			ps = connection.prepareStatement(sql);
			ps.setString(1,admin.getAdmin_name());
			ps.setString(2,admin.getAdmin_email());
			ps.setString(3,admin.getAdmin_password());
			ps.setInt(4,admin.getAdmin_id());
			ps.execute();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean deleteAdmin(int admin_id)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql="DELETE FROM admin WHERE admin_id=?";
			ps=connection.prepareStatement(sql);		
			ps.setInt(1, admin_id);;
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
}
