package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import com.enparadigm.activities.beans.Program_question_mapping;
import com.enparadigm.activities.connection.DatabaseConnection;


public class Program_question_mappingDAO {
	
	

	public ArrayList<Program_question_mapping> fetchProgram_question_mappingList() 
	{
		DatabaseConnection dbConnection = DatabaseConnection.getInstance();
		Connection connection = dbConnection.getConnection();
		ArrayList<Program_question_mapping> program_question_mappingList = new ArrayList<Program_question_mapping>();
		try{
			String sql = "select program_id,question_id,active from program_question_mapping order by program_id,question_id;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Program_question_mapping program_question_mapping = new Program_question_mapping();
				program_question_mapping.setProgram_id(rs.getInt("program_id"));
				program_question_mapping.setQuestion_id(rs.getInt("question_id"));
				program_question_mapping.setActive(rs.getInt("active"));

				program_question_mappingList.add(program_question_mapping);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			dbConnection.releaseConnection(connection);
		}
		return program_question_mappingList;
	}
	public ArrayList<Program_question_mapping> fetchProgram_question_mappingList(int program_id) 
	{
		DatabaseConnection dbConnection = DatabaseConnection.getInstance();
		Connection connection = dbConnection.getConnection();
		ArrayList<Program_question_mapping> program_question_mappingList = new ArrayList<Program_question_mapping>();
		try{
			String sql = "select program_id,question_id,active from program_question_mapping where program_id=? order by program_id,question_id;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, program_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Program_question_mapping program_question_mapping = new Program_question_mapping();
				program_question_mapping.setProgram_id(rs.getInt("program_id"));
				program_question_mapping.setQuestion_id(rs.getInt("question_id"));
				program_question_mapping.setActive(rs.getInt("active"));

				program_question_mappingList.add(program_question_mapping);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			dbConnection.releaseConnection(connection);
		}
		return program_question_mappingList;
	}
	
	public Program_question_mapping fetchProgram_question_mapping(int program_id,int question_id) 
	{
		DatabaseConnection dbConnection = DatabaseConnection.getInstance();
		Connection connection = dbConnection.getConnection();
		Program_question_mapping program_question_mapping = new Program_question_mapping();
		try{
			String sql = "select program_id,question_id,active from program_question_mapping where program_id=? and question_id=? order by program_id,question_id;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,program_id);
			ps.setInt(2,question_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				program_question_mapping.setProgram_id(rs.getInt("program_id"));
				program_question_mapping.setQuestion_id(rs.getInt("question_id"));
				program_question_mapping.setActive(rs.getInt("active"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			dbConnection.releaseConnection(connection);
		}
		return program_question_mapping;
	}
	public boolean insertProgram_question_mapping(Program_question_mapping program_question_mapping) 
	{
		boolean status=true;
		DatabaseConnection dbConnection = DatabaseConnection.getInstance();
		Connection connection = dbConnection.getConnection();
		try{
			String sql = "insert into program_question_mapping (program_id,question_id,active) values(?,?,?) ;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
				ps.setInt(1,program_question_mapping.getProgram_id());
				ps.setInt(2,program_question_mapping.getQuestion_id());
				ps.setInt(3,program_question_mapping.getActive());
			ps.execute();
		}catch(SQLException e){
			status=false;
			e.printStackTrace();
		}
		finally{
			dbConnection.releaseConnection(connection);
		}
		return status;
	}
	public boolean updateProgram_question_mapping(Program_question_mapping program_question_mapping) 
	{
		boolean status=true;
		DatabaseConnection dbConnection = DatabaseConnection.getInstance();
		Connection connection = dbConnection.getConnection();
		try{
			String sql = "update program_question_mapping active=? where program_id=?,question_id=?"; 
			PreparedStatement ps = connection.prepareStatement(sql);
				ps.setString(1,Integer.toString(program_question_mapping.getProgram_id()));
				ps.setString(2,Integer.toString(program_question_mapping.getQuestion_id()));
				ps.setString(3,Integer.toString(program_question_mapping.getActive()));
			ps.execute();
		}catch(SQLException e){
			status=false;
			e.printStackTrace();
		}
		finally{
			dbConnection.releaseConnection(connection);
		}
		return status;
	}
	public boolean deleteProgram_question_mapping(int program_id,int question_id) 
	{
		boolean status=true;
		DatabaseConnection dbConnection = DatabaseConnection.getInstance();
		Connection connection = dbConnection.getConnection();
		try{
			String sql = "delete from program_question_mapping where program_id=? and question_id=?;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
				ps.setString(1,Integer.toString(program_id));
				ps.setString(2,Integer.toString(question_id));
				ps.execute();
		}catch(SQLException e){
			status=false;
			e.printStackTrace();
		}
		finally{
			dbConnection.releaseConnection(connection);
		}
		return status;
	}
}