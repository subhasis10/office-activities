package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enparadigm.activities.beans.Feedback_question;
import com.enparadigm.activities.connection.DatabaseConnection;

public class Feedback_questionDAO {
	
	public static void main(String args[])
	{
//		Feedback_question feedback_question = new Feedback_question();
		
//		int question_id = 4;
//		String question = "Amount of fun you had in the activities";
		
//		feedback_question.setQuestion_id(question_id);
//		feedback_question.setQuestion(question);
//		
//		feedback_question = fetchFeedback_question(question_id);
//		System.out.println(feedback_question.getQuestion_id());
//		System.out.println(feedback_question.getQuestion());
		
//		boolean status = false;
//		status = insertFeedback_question(feedback_question);
//		status = deleteFeedback_question(question_id);
//		status = updateFeedback_question(feedback_question);
//		if(status == true)
//			System.out.println("Successful");
//		else
//			System.out.println("Unsuccessful");
		
//		ArrayList<Feedback_question> feedback_questionList = fetchFeedback_questionList();
//		for(Feedback_question feedback_question1 : feedback_questionList)
//		{
//			System.out.println(feedback_question1.getQuestion_id());
//			System.out.println(feedback_question1.getQuestion());
//		}
		
		return;
	}
	
	public ArrayList<Feedback_question> fetchFeedback_questionList() 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		ArrayList<Feedback_question> feedback_questionList = new ArrayList<Feedback_question>();
		try{
			String sql = "SELECT * FROM feedback_question ORDER BY question_id;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Feedback_question feedback_question = new Feedback_question();
				feedback_question.setQuestion_id(rs.getInt("question_id"));
				feedback_question.setQuestion(rs.getString("question"));

				feedback_questionList.add(feedback_question);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return feedback_questionList;
	}
	
	public Feedback_question fetchFeedback_question(int question_id) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		Feedback_question feedback_question = new Feedback_question();
		try{
			String sql = "SELECT * FROM feedback_question WHERE question_id=?"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,question_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				feedback_question.setQuestion_id(rs.getInt("question_id"));
				feedback_question.setQuestion(rs.getString("question"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return feedback_question;
	}
	
	public boolean insertFeedback_question(Feedback_question feedback_question)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "INSERT INTO feedback_question(question) VALUES (?)";
			ps = connection.prepareStatement(sql);
			ps.setString(1, feedback_question.getQuestion());
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean updateFeedback_question(Feedback_question feedback_question)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql = "UPDATE feedback_question SET question=? WHERE question_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setString(1, feedback_question.getQuestion());
			ps.setInt(2, feedback_question.getQuestion_id());;
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean deleteFeedback_question(int question_id)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql = "DELETE FROM feedback_question WHERE question_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setInt(1, question_id);;
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
}
