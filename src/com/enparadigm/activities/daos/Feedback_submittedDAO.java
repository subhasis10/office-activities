package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enparadigm.activities.beans.Feedback_submitted;
import com.enparadigm.activities.connection.DatabaseConnection;

public class Feedback_submittedDAO {
	
	public static void main(String args[])
	{
		Feedback_submitted feedback_submitted = new Feedback_submitted();
		Feedback_submittedDAO objFeedback_submittedDAO = new Feedback_submittedDAO();
//		
//		int feedback_id = 3;
		String name = "Sunil";
		String feedback = "hagjhagjdghjas";
		int question1_id = 1;
		int question2_id = 4;
		int question3_id = 5;
		int rating1 = 6;
		int rating2 = 8;
		int rating3 = 7;
		int program_id = 2;
//		
//		feedback_submitted.setFeedback_id(feedback_id);
		feedback_submitted.setName(name);
		feedback_submitted.setFeedback(feedback);
		feedback_submitted.setQuestion1_id(question1_id);
		feedback_submitted.setQuestion2_id(question2_id);
		feedback_submitted.setQuestion3_id(question3_id);
		feedback_submitted.setRating1(rating1);
		feedback_submitted.setRating2(rating2);
		feedback_submitted.setRating3(rating3);
		feedback_submitted.setProgram_id(program_id);
//		
//		feedback_submitted = objFeedback_submittedDAO.fetchFeedback_submitted(feedback_id);
//		System.out.println(feedback_submitted.getFeedback_id());
//		System.out.println(feedback_submitted.getName());
//		System.out.println(feedback_submitted.getFeedback());
//		System.out.println(feedback_submitted.getQuestion1_id());
//		System.out.println(feedback_submitted.getQuestion2_id());
//		System.out.println(feedback_submitted.getQuestion3_id());
//		System.out.println(feedback_submitted.getRating1());
//		System.out.println(feedback_submitted.getRating2());
//		System.out.println(feedback_submitted.getRating3());
//		System.out.println(feedback_submitted.getProgram_id());
//		System.out.println(feedback_submitted.getCreated_at());
//		
		boolean status = false;
		status = objFeedback_submittedDAO.insertFeedback_submitted(feedback_submitted);
//		status = deleteFeedback_submitted(feedback_id);
//		status = updateFeedback_submitted(feedback_submitted);
		if(status == true)
			System.out.println("Successful");
		else
			System.out.println("Unsuccessful");
		
//		ArrayList<Feedback_submitted> feedback_submittedList = objFeedback_submittedDAO.fetchFeedback_submittedList();
//		ArrayList<Feedback_submitted> feedback_submittedList = objFeedback_submittedDAO.fetchFeedback_submittedListByProgram_id(program_id);
//		for(Feedback_submitted feedback_submitted1 : feedback_submittedList)
//		{
//			System.out.println(feedback_submitted1.getFeedback_id());
//			System.out.println(feedback_submitted1.getName());
//			System.out.println(feedback_submitted1.getFeedback());
//			System.out.println(feedback_submitted1.getQuestion1_id());
//			System.out.println(feedback_submitted1.getQuestion2_id());
//			System.out.println(feedback_submitted1.getQuestion3_id());
//			System.out.println(feedback_submitted1.getRating1());
//			System.out.println(feedback_submitted1.getRating2());
//			System.out.println(feedback_submitted1.getRating3());
//			System.out.println(feedback_submitted1.getProgram_id());
//			System.out.println(feedback_submitted1.getCreated_at());
//		}
		
		return;
	}
	
	public ArrayList<Feedback_submitted> fetchFeedback_submittedList() 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		ArrayList<Feedback_submitted> feedback_submittedList = new ArrayList<Feedback_submitted>();
		try{
			String sql = "select * from feedback_submitted order by feedback_id;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Feedback_submitted feedback_submitted = new Feedback_submitted();
				feedback_submitted.setFeedback_id(rs.getInt("feedback_id"));
				feedback_submitted.setName(rs.getString("name"));
				feedback_submitted.setFeedback(rs.getString("feedback"));
				feedback_submitted.setQuestion1_id(rs.getInt("question1_id"));
				feedback_submitted.setQuestion2_id(rs.getInt("question2_id"));
				feedback_submitted.setQuestion3_id(rs.getInt("question3_id"));
				feedback_submitted.setRating1(rs.getInt("rating1"));
				feedback_submitted.setRating2(rs.getInt("rating2"));
				feedback_submitted.setRating3(rs.getInt("rating3"));
				feedback_submitted.setProgram_id(rs.getInt("program_id"));
				feedback_submitted.setCreated_at(rs.getString("created_at"));

				feedback_submittedList.add(feedback_submitted);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return feedback_submittedList;
	}
	
	public Feedback_submitted fetchFeedback_submitted(int feedback_id) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		Feedback_submitted feedback_submitted = new Feedback_submitted();
		try
		{
			String sql = "select * from feedback_submitted where feedback_id=? order by feedback_id"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,feedback_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				feedback_submitted.setFeedback_id(rs.getInt("feedback_id"));
				feedback_submitted.setName(rs.getString("name"));
				feedback_submitted.setFeedback(rs.getString("feedback"));
				feedback_submitted.setQuestion1_id(rs.getInt("question1_id"));
				feedback_submitted.setQuestion2_id(rs.getInt("question2_id"));
				feedback_submitted.setQuestion3_id(rs.getInt("question3_id"));
				feedback_submitted.setRating1(rs.getInt("rating1"));
				feedback_submitted.setRating2(rs.getInt("rating2"));
				feedback_submitted.setRating3(rs.getInt("rating3"));
				feedback_submitted.setProgram_id(rs.getInt("program_id"));
				feedback_submitted.setCreated_at(rs.getString("created_at"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return feedback_submitted;
	}
	
	public ArrayList<Feedback_submitted> fetchFeedback_submittedListByProgram_id(int program_id) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		ArrayList<Feedback_submitted> feedback_submittedList = new ArrayList<Feedback_submitted>();
		try{
			String sql = "select * from feedback_submitted order by feedback_id where program_id=?;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,program_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Feedback_submitted feedback_submitted = new Feedback_submitted();
				feedback_submitted.setFeedback_id(rs.getInt("feedback_id"));
				feedback_submitted.setName(rs.getString("name"));
				feedback_submitted.setFeedback(rs.getString("feedback"));
				feedback_submitted.setQuestion1_id(rs.getInt("question1_id"));
				feedback_submitted.setQuestion2_id(rs.getInt("question2_id"));
				feedback_submitted.setQuestion3_id(rs.getInt("question3_id"));
				feedback_submitted.setRating1(rs.getInt("rating1"));
				feedback_submitted.setRating2(rs.getInt("rating2"));
				feedback_submitted.setRating3(rs.getInt("rating3"));
				feedback_submitted.setProgram_id(rs.getInt("program_id"));
				feedback_submitted.setCreated_at(rs.getString("created_at"));

				feedback_submittedList.add(feedback_submitted);
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return feedback_submittedList;
	}
	
	public boolean insertFeedback_submitted(Feedback_submitted feedback_submitted)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "INSERT INTO feedback_submitted(name,feedback,question1_id,question2_id,question3_id,rating1,rating2,rating3,program_id) VALUES (?,?,?,?,?,?,?,?,?)";
			ps = connection.prepareStatement(sql);
			ps.setString(1,feedback_submitted.getName());
			ps.setString(2,feedback_submitted.getFeedback());
			ps.setInt(3,feedback_submitted.getQuestion1_id());
			ps.setInt(4,feedback_submitted.getQuestion2_id());
			ps.setInt(5,feedback_submitted.getQuestion3_id());
			ps.setInt(6,feedback_submitted.getRating1());
			ps.setInt(7,feedback_submitted.getRating2());
			ps.setInt(8,feedback_submitted.getRating3());
			ps.setInt(9,feedback_submitted.getProgram_id());
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean updateFeedback_submitted(Feedback_submitted feedback_submitted)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql = "UPDATE feedback_submitted SET name=?, feedback=?, question1_id=?, question2_id=?, question3_id=?, rating1=?, rating2=?, rating3=?, program_id=? WHERE feedback_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setString(1,feedback_submitted.getName());
			ps.setString(2,feedback_submitted.getFeedback());
			ps.setInt(3,feedback_submitted.getQuestion1_id());
			ps.setInt(4,feedback_submitted.getQuestion2_id());
			ps.setInt(5,feedback_submitted.getQuestion3_id());
			ps.setInt(6,feedback_submitted.getRating1());
			ps.setInt(7,feedback_submitted.getRating2());
			ps.setInt(8,feedback_submitted.getRating3());
			ps.setInt(9,feedback_submitted.getProgram_id());
			ps.setInt(10,feedback_submitted.getFeedback_id());
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean deleteFeedback_submitted(int feedback_id)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql = "DELETE FROM feedback_submitted WHERE feedback_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setInt(1, feedback_id);;
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
}
