package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enparadigm.activities.beans.Category;
import com.enparadigm.activities.connection.DatabaseConnection;

public class CategoryDAO {
	
	public static void main(String args[])
	{
//		Category category = new Category();
//		
//		int category_id = 6;
//		String category_name = "Something";
//		
//		category.setCategory_id(category_id);
//		category.setCategory_name(category_name);
//		
//		category = fetchCategoryById(category_id);
//		System.out.println(category.getCategory_id());
//		System.out.println(category.getCategory_name());
//		
//		boolean status = false;
//		status = insertCategory(category);
//		status = deleteCategory(category_id);
//		status = updateCategory(category);
//		if(status == true)
//			System.out.println("Successful");
//		else
//			System.out.println("Unsuccessful");
		
//		ArrayList<Category> categoryList = fetchCategoryList();
//		for(Category category1 : categoryList)
//		{
//			System.out.println(category1.getCategory_id());
//			System.out.println(category1.getCategory_name());
//		}
		
		return;
	}
	
	public ArrayList<Category> fetchCategoryList() 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		ArrayList<Category> categoryList = new ArrayList<Category>();
		try{
			String sql = "SELECT * FROM category ORDER BY category_id;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Category category = new Category();
				category.setCategory_id(rs.getInt("category_id"));
				category.setCategory_name(rs.getString("category_name"));

				categoryList.add(category);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return categoryList;
	}
	
	public Category fetchCategoryByName(String category_name) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		Category category = new Category();
		try{
			String sql = "SELECT * FROM category WHERE category_name=?"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1,category_name);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				category.setCategory_id(rs.getInt("category_id"));
				category.setCategory_name(rs.getString("category_name"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return category;
	}
	
	public Category fetchCategoryById(int category_id) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		Category category = new Category();
		try{
			String sql = "SELECT * FROM category WHERE category_id=?"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,category_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				category.setCategory_id(rs.getInt("category_id"));
				category.setCategory_name(rs.getString("category_name"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return category;
	}
	
	public boolean insertCategory(Category category)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "INSERT INTO category(category_name) VALUES (?)";
			ps = connection.prepareStatement(sql);
			ps.setString(1, category.getCategory_name());
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean updateCategory(Category category)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql = "UPDATE category SET category_name=? WHERE category_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setString(1, category.getCategory_name());
			ps.setInt(2, category.getCategory_id());;
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean deleteCategory(int category_id)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql = "DELETE FROM category WHERE category_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setInt(1, category_id);;
			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
}
