package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enparadigm.activities.beans.Category_count;
import com.enparadigm.activities.connection.DatabaseConnection;

public class Category_countDAO {
	
	public static void main(String args[])
	{
		
//		ArrayList<Category_count> category_countList = fetchCategory_countList();
//		for(Category_count category_count : category_countList)
//		{
//			System.out.println(category_count.getCategory_name());
//			System.out.println(category_count.getSuggestion_count());
//		}
		return;
	}
	
	public ArrayList<Category_count> fetchCategory_countList() 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		ArrayList<Category_count> category_countList = new ArrayList<Category_count>();
		try{
			String sql = "SELECT c.category_name, count(s.category_id) suggestion_count FROM category c LEFT JOIN suggestion s on c.category_id = s.category_id GROUP BY c.category_name"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Category_count category_count = new Category_count();
				category_count.setCategory_name(rs.getString("category_name"));
				category_count.setSuggestion_count(rs.getInt("suggestion_count"));

				category_countList.add(category_count);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return category_countList;
	}
}
