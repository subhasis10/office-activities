package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enparadigm.activities.beans.Feedback_question;
import com.enparadigm.activities.connection.DatabaseConnection;

public class FetchFeedbackProgramQuestionsDAO {
	public ArrayList<Feedback_question> fetchFeedback_programquestionList(String program_name) 
	{
		DatabaseConnection dbConnection = DatabaseConnection.getInstance();
		Connection connection = dbConnection.getConnection();
		ArrayList<Feedback_question> feedback_questionList = new ArrayList<Feedback_question>();
		try{
			String sql = "select Q.question_id,Q.question from program_meta P inner join program_question_mapping PQ on P.program_id=PQ.program_id inner join feedback_question Q on PQ.question_id=Q.question_id where P.program_name=? and PQ.active=1;"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, program_name);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Feedback_question feedback_question = new Feedback_question();
				feedback_question.setQuestion_id(rs.getInt("question_id"));
				feedback_question.setQuestion(rs.getString("question"));

				feedback_questionList.add(feedback_question);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			dbConnection.releaseConnection(connection);
		}
		return feedback_questionList;
	}
}
