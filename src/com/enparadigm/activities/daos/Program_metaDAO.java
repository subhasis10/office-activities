package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.google.gson.Gson; 
import com.google.gson.GsonBuilder; 

import com.enparadigm.activities.beans.Program_meta;
import com.enparadigm.activities.connection.DatabaseConnection;

public class Program_metaDAO {
	
	public static void main(String args[])
	{
		Program_meta objProgram_meta = new Program_meta();
		Program_metaDAO objProgram_metaDAO = new Program_metaDAO();
//		
		int program_id = 2;
//		String program_name = "Republic Day";
//		String strDate = "2019-01-26";
//		Date program_date = Date.valueOf(strDate);
//		
//		objProgram_meta.setProgram_id(program_id);
//		objProgram_meta.setProgram_name(program_name);
//		objProgram_meta.setProgram_date(program_date);
//		
		objProgram_meta = objProgram_metaDAO.fetchProgram_meta(program_id);
		GsonBuilder builder = new GsonBuilder(); 
	    builder.setPrettyPrinting();
	    Gson gson = builder.create(); 
	    
	    String jsonString = gson.toJson(objProgram_meta); 
	    System.out.println(jsonString); 
	    
//		System.out.println(objProgram_meta.getProgram_id());
//		System.out.println(objProgram_meta.getProgram_name());
//		System.out.println(objProgram_meta.getProgram_date());
//		
//		boolean status = false;
//		status = insertProgram_meta(objProgram_meta);
//		status = deleteProgram_meta(program_id);
//		status = updateProgram_meta(objProgram_meta);
//		if(status == true)
//			System.out.println("Successful");
//		else
//			System.out.println("Unsuccessful");
		
//		ArrayList<Program_meta> Program_metaList = objProgram_metaDAO.fetchProgram_metaList();
//		for(Program_meta program_meta : Program_metaList)
//		{
//			System.out.println(program_meta.getProgram_id());
//			System.out.println(program_meta.getProgram_name());
//			System.out.println(program_meta.getProgram_date());
//		}
		return;
	}
	
	public ArrayList<Program_meta> fetchProgram_metaList() 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		ArrayList<Program_meta> program_metaList = new ArrayList<Program_meta>();
		try{
			String sql = "SELECT * FROM program_meta ORDER BY program_id"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				Program_meta program_meta = new Program_meta();
				program_meta.setProgram_id(rs.getInt("program_id"));
				program_meta.setProgram_name(rs.getString("program_name"));
				program_meta.setProgram_date(rs.getDate("program_date"));

				program_metaList.add(program_meta);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return program_metaList;
	}
	
	public Program_meta fetchProgram_meta(int program_id) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		Program_meta program_meta = new Program_meta();
		try
		{
			String sql = "SELECT * FROM program_meta WHERE program_id=?"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,program_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				program_meta.setProgram_id(rs.getInt("program_id"));
				program_meta.setProgram_name(rs.getString("program_name"));
				program_meta.setProgram_date(rs.getDate("program_date"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return program_meta;
	}
	
	public boolean insertProgram_meta(Program_meta program_meta)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try 
		{
			String sql = "INSERT into program_meta(program_name, program_date) values (?,?)";
			ps = connection.prepareStatement(sql);
			ps.setString(1, program_meta.getProgram_name());
			ps.setDate(2, program_meta.getProgram_date());
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean updateProgram_meta(Program_meta program_meta)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try 
		{
			String sql = "UPDATE program_meta SET program_name=?, program_date=? WHERE program_id=?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, program_meta.getProgram_name());
			ps.setDate(2, program_meta.getProgram_date());
			ps.setInt(3, program_meta.getProgram_id());
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean deleteProgram_meta(int program_id)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql="DELETE FROM program_meta WHERE program_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setInt(1, program_id);;
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
}
