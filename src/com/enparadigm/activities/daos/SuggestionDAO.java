package com.enparadigm.activities.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.enparadigm.activities.beans.Suggestion;
import com.enparadigm.activities.connection.DatabaseConnection;

public class SuggestionDAO {
	
	public static void main(String args[])
	{
		SuggestionDAO objSuggestionDAO = new SuggestionDAO();
		Suggestion objSuggestion = new Suggestion();
//		
//		int suggestion_id = 5;
		String suggester_name = "randomaaba";
		String suggestion = "wertyuesgrdhblhjasbhlkasbdhbashkdbashklbdhlkas";
		int category_id = 2;
		
//		objSuggestion.setSuggestion_id(suggestion_id);
		objSuggestion.setSuggester_name(suggester_name);
		objSuggestion.setSuggestion(suggestion);
		objSuggestion.setCategory_id(category_id);
		
//		objSuggestion = fetchSuggestion(suggestion_id);
//		System.out.println(objSuggestion.getSuggestion_id());
//		System.out.println(objSuggestion.getSuggester_name());
//		System.out.println(objSuggestion.getSuggestion());
//		System.out.println(objSuggestion.getCategory_id());
//		System.out.println(Suggestion1.getCreated_at());
		
		boolean status = false;
		status = objSuggestionDAO.insertSuggestion(objSuggestion);
//		status = objSuggestionDAO.deleteSuggestion(suggestion_id);
//		status = objSuggestionDAO,updateSuggestion(objSuggestion);
		if(status == true)
			System.out.println("Successful");
		else
			System.out.println("Unsuccessful");
		
//		ArrayList<Suggestion> SuggestionList = objSuggestionDAO.fetchSuggestionList();
//		for(Suggestion Suggestion1 : SuggestionList)
//		{
//			System.out.println(Suggestion1.getSuggestion_id());
//			System.out.println(Suggestion1.getSuggester_name());
//			System.out.println(Suggestion1.getSuggestion());
//			System.out.println(Suggestion1.getCategory_id());
//			System.out.println(Suggestion1.getCreated_at());
//		}
		return;
	}
	
	public ArrayList<Suggestion> fetchSuggestionList() 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		ArrayList<Suggestion> suggestionList = new ArrayList<Suggestion>();
		try{
			String sql = "SELECT * FROM suggestion ORDER BY suggestion_id"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Suggestion suggestion = new Suggestion();
				suggestion.setSuggestion_id(rs.getInt("suggestion_id"));
				suggestion.setSuggester_name(rs.getString("suggester_name"));
				suggestion.setSuggestion(rs.getString("suggestion"));
				suggestion.setCategory_id(rs.getInt("category_id"));
				suggestion.setCreated_at(rs.getString("created_at"));

				suggestionList.add(suggestion);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			objDatabaseConnection.releaseConnection(connection);
		}
		return suggestionList;
	}
	
	public Suggestion fetchSuggestion(int suggestion_id) 
	{
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		Suggestion suggestion = new Suggestion();
		try{
			String sql = "SELECT * FROM suggestion WHERE suggestion_id=?"; 
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1,suggestion_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				suggestion.setSuggestion_id(rs.getInt("suggestion_id"));
				suggestion.setSuggester_name(rs.getString("suggester_name"));
				suggestion.setSuggestion(rs.getString("suggestion"));
				suggestion.setCategory_id(rs.getInt("category_id"));
				suggestion.setCreated_at(rs.getString("created_at"));
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return suggestion;
	}
	
	public boolean insertSuggestion(Suggestion suggestion)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "INSERT into suggestion(suggester_name, suggestion, category_id) values (?,?,?)";
			ps = connection.prepareStatement(sql);
			ps.setString(1, suggestion.getSuggester_name());
			ps.setString(2, suggestion.getSuggestion());
			ps.setInt(3, suggestion.getCategory_id());
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean updateSuggestion(Suggestion suggestion)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "UPDATE suggestion SET suggester_name=?, suggestion=?, category_id=? WHERE suggestion_id=?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, suggestion.getSuggester_name());
			ps.setString(2, suggestion.getSuggestion());
			ps.setInt(3, suggestion.getCategory_id());
			ps.setInt(4, suggestion.getSuggestion_id());
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
	
	public boolean deleteSuggestion(int suggestion_id)
	{
		boolean status = true;
		DatabaseConnection objDatabaseConnection = DatabaseConnection.getInstance();
		Connection connection = objDatabaseConnection.getConnection();
		PreparedStatement ps = null;
		try
		{
			String sql="DELETE FROM suggestion WHERE suggestion_id=?";
			ps = connection.prepareStatement(sql);		
			ps.setInt(1, suggestion_id);;
			ps.executeUpdate();
		}
		catch(Exception e)
		{
			status = false;
			e.printStackTrace();
		}
		finally
		{
			objDatabaseConnection.releaseConnection(connection);
		}
		return status;
	}
}
