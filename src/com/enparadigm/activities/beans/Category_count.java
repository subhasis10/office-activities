package com.enparadigm.activities.beans;

public class Category_count {
	
	private String category_name;
	private int suggestion_count;
	
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public int getSuggestion_count() {
		return suggestion_count;
	}
	public void setSuggestion_count(int suggestion_count) {
		this.suggestion_count = suggestion_count;
	}
	
}
