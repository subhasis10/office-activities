package com.enparadigm.activities.beans;

public class Feedback_submitted {
	
	private int feedback_id;
	private String name;
	private String feedback;
	private int question1_id;
	private int question2_id;
	private int question3_id;
	private int rating1;
	private int rating2;
	private int rating3;
	private int program_id;
	private String created_at;

	public int getFeedback_id()
	{
		return feedback_id;
	}
	public void setFeedback_id(int feedback_id)
	{
		this.feedback_id = feedback_id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getFeedback()
	{
		return feedback;
	}
	public void setFeedback(String feedback)
	{
		this.feedback = feedback;
	}
	public int getQuestion1_id()
	{
		return question1_id;
	}
	public void setQuestion1_id(int question1_id)
	{
		this.question1_id = question1_id;
	}
	public int getQuestion2_id(){
		return question2_id;
	}
	public void setQuestion2_id(int question2_id)
	{
		this.question2_id = question2_id;
	}
	public int getQuestion3_id(){
		return question3_id;
	}
	public void setQuestion3_id(int question3_id)
	{
		this.question3_id = question3_id;
	}
	public int getRating1()
	{
		return rating1;
	}
	public void setRating1(int rating1)
	{
		this.rating1 = rating1;
	}
	public int getRating2(){
		return rating2;
	}
	public void setRating2(int rating2)
	{
		this.rating2 = rating2;
	}
	public int getRating3()
	{
		return rating3;
	}
	public void setRating3(int rating3)
	{
		this.rating3 = rating3;
	}
	public int getProgram_id()
	{
		return program_id;
	}
	public void setProgram_id(int program_id)
	{
		this.program_id = program_id;
	}
	public String getCreated_at()
	{
		return created_at;
	}
	public void setCreated_at(String created_at)
	{
		this.created_at = created_at;
	}
}
