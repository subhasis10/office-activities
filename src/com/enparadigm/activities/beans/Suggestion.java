package com.enparadigm.activities.beans;

public class Suggestion {
	private int suggestion_id;
	private String suggester_name;
	private String suggestion;
	private int category_id;
	private String created_at;
	
	public int getSuggestion_id()
	{
		return suggestion_id;
	}
	public String getSuggester_name()
	{
		return suggester_name;
	}
	public String getSuggestion()
	{
		return suggestion;
	}
	public int getCategory_id()
	{
		return category_id;
	}
	public void setSuggestion_id(int suggestion_id)
	{
		this.suggestion_id = suggestion_id;
	}
	public void setSuggester_name(String suggester_name)
	{
		this.suggester_name = suggester_name;
	}
	public void setSuggestion(String suggestion)
	{
		this.suggestion = suggestion;
	}
	public void setCategory_id(int category_id)
	{
		this.category_id = category_id;
	}
	public String getCreated_at()
	{
		return created_at;
	}
	public void setCreated_at(String created_at)
	{
		this.created_at = created_at;
	}
}
