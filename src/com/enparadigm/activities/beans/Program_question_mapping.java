package com.enparadigm.activities.beans;



public class Program_question_mapping{
	private int program_id;
	private int question_id;
	private int active;

	public int getProgram_id(){
		return program_id;
	}
	public void setProgram_id(int program_id){
		this.program_id = program_id;
	}
	public int getQuestion_id(){
		return question_id;
	}
	public void setQuestion_id(int question_id){
		this.question_id = question_id;
	}
	public int getActive(){
		return active;
	}
	public void setActive(int active){
		this.active = active;
	}
}

