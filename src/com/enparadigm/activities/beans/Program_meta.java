package com.enparadigm.activities.beans;

import java.sql.Date;

public class Program_meta {
	
	private int program_id;
	private String program_name;
	private Date program_date;

	public int getProgram_id()
	{
		return program_id;
	}
	public void setProgram_id(int program_id)
	{
		this.program_id = program_id;
	}
	public String getProgram_name()
	{
		return program_name;
	}
	public void setProgram_name(String program_name)
	{
		this.program_name = program_name;
	}
	public Date getProgram_date()
	{
		return program_date;
	}
	public void setProgram_date(Date program_date)
	{
		this.program_date = program_date;
	}
}
