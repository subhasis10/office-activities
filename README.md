# Office Activities 
Office Activities is a web application for upcoming festive events and activities where users can give their own suggestions and feedbacks.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)