<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.enparadigm.activities.daos.Program_metaDAO"%>
<%@page import="com.enparadigm.activities.beans.Program_meta"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Feedback Results</title>
<script src="https://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
	$("select").on('change', function() {
		var program_id = this.value;
		$.get('GetDateServlet', {
            program_id : program_id
	    }, function(responseText) {
	    		var obj = JSON.parse(responseText);
	            //document.getElementById("program_date").innerHTML = this.responseText;
	    		$('#program-date').text(obj.program_date);
	    });
	});
});
	
</script>
<style>
	.header
	{
		text-align: center;
	}
	select {
	  width: 100%;
	  padding: 12px 20px;
	  margin: 8px 0;
	  display: inline-block;
	  border: 1px solid #ccc;
	  border-radius: 4px;
	  box-sizing: border-box;
	}
	.program-div {
		width: 50%;
		border-radius: 5px;
		background-color: #f2f2f2;
		padding: 20px;
		margin: 0 auto;
	}
</style>
</head>
<body>
	
	<div class="header"> <h1> Feedback Results </h1> </div>
	<div class="program-div">
		<label for="program_name">Programs</label>
		<select id="program_name" name="program_name">
	    <%
			Program_metaDAO objProgram_metaDAO = new Program_metaDAO();
			ArrayList<Program_meta> Program_metaList = objProgram_metaDAO.fetchProgram_metaList();
			for(Program_meta program_meta : Program_metaList)
			{
    	%>
				<option value=<%= program_meta.getProgram_id() %>> <%= program_meta.getProgram_name() %> </option>
	    <%
    		}
	    %>
	    </select>
	</div>
	<div id="program-date"></div>
</body>
</html>