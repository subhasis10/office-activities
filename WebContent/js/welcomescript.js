$("document").ready(function(){
	var feedbackprogramid;
	var feedbackquestion1id;
	var feedbackquestion2id;
	var feedbackquestion3id;
	var suggestioncategoryid;
	getUpcomingPrograms();
	getPastPrograms();
	getCategory();
});

function getUpcomingPrograms()
{
	document.getElementById('currentDate').value=new Date().toISOString().slice(0, 10);
	//alert("getAll working");
	$.ajax({
		method:'POST',
		url:'FetchUpcomingProgramListServlet',
		dataType:'json',
		data:
		{
			"currentdate":$("#currentDate").val()
		},
		success:function(data)
		{
	
			var trHTML = '';
			var index=0;
			$('#upcomingprogramlist-table tbody tr').remove();
	        $.each(data, function (){ 
	        {
	        
	            trHTML += '<tr><td>'+data[index].program_name+'</td><td>'+data[index].program_date+'</td></tr>';
	            index++;
	        
	    	}
	        });
	        $('#upcomingprogramlist-table').append(trHTML);
		},
		error:function()
			{
			alert("error ajax in getUpcomingPrograms() welcomescript.js");
			}
	});
	return false;
};
function getPastPrograms()
{
	document.getElementById('currentDate').value=new Date().toISOString().slice(0, 10);
	//alert("getAll working");
	$.ajax({
		method:'POST',
		url:'FetchPastProgramListServlet',
		dataType:'json',
		data:
		{
			"currentdate":$("#currentDate").val()
		},
		success:function(data)
		{
	
			var optionHTML ='';
			var index=data.length-1;
			$('#feedbackprogramlist option').remove();
	        $.each(data, function (){ 
	        {
	        
	            optionHTML += '<option>'+data[index].program_name+'</option>';
	            index--;
	        
	    	}
	        });
	        $('#feedbackprogramlist').append('<option selected disabled>Choose a program</option>');
	        $('#feedbackprogramlist').append(optionHTML);
	        
		},
		error:function()
			{
			alert("error ajax in getPastPrograms() welcomescript.js");
			}
	});
	return false;
};

function getFeedbackQuestions(feedbackprogramlist)
{
	$.ajax({
		method:'POST',
		url:'FetchProgramQuestionsServlet',
		dataType:'json',
		data:
		{
			"program_name":$("#feedbackprogramlist").val()
		},
		success:function(data)
		{		
			
			var questionHTML = '';
			var index=0;
			$('#feedbackquestionlist div').remove();
	        $.each(data, function (){ 
	        {
	        
	            questionHTML += '<div class="row-xs-2"><p id="question'+index+1+'" >'+data[index].question+'</p><input type="range" class="custom-range" id="slider'+index+1+'" min="0" max="10" onchange="setFeedbackRatings('+index+1+',this.value);"><input type="hidden" id="rating'+index+1+'"></div>' ;
	            index++;
	        
	    	}
	        });
	        $('#feedbackquestionlist').append(questionHTML);
	        window.feedbackprogramid=$("#feedbackprogramlist").val();
	        window.feedbackquestion1id=feedbackquestion1id=data[0].question_id;
	        window.feedbackquestion1id=feedbackquestion2id=data[1].question_id;
	        window.feedbackquestion1id=feedbackquestion3id=data[2].question_id;
	        
	        
		},
		error:function()
			{
			alert("error ajax in getFeedbackQuestions() welcomescript.js");
			}
	});
	return false;
};

function setFeedbackRatings(id,ratingval)
{
	$('"#rating'+id+'"').val(ratingval);
	alert($('"#rating'+id+'"').val());
}


$("#loginbtn").click(function(){
	$.ajax({
		method:'POST',
		url:'AdminAuthenticationServlet',
		dataType:'json',
		data:{
			"name":$("#name").val(),
			"password":$("#password").val(),
		},
		success:function(data)
		{
			alert("ajax success");
			if(data.admin==$("#name").val())
				{
				$('#loginmodal').hide();
				$('.modal-backdrop').hide();
				window.location="http://localhost:8080/officeactivity/adminhome.jsp";
			
				}
			else
				{
				//window.location="http://localhost:8080/officeactivity/welcome.jsp";
				
				}
					
				
		},
		error:function()
			{
			alert("error ajax in loginbtn");
			$("#loginmodal").modal("hide");
			$('.modal-backdrop').hide();
			}
		
	});
	return false;
	
});
function getCategory()
{
	//document.getElementById('currentDate').value=new Date().toISOString().slice(0, 10);
	//alert("getAll working");
	$.ajax({
		method:'GET',
		url:'FetchCategoryListServlet',
		/*dataType:'json',
		data:
		{
			"currentdate":$("#currentDate").val()
		},
		*/
		success:function(data)
		{
	
			var optionHTML ='';
			var index=0;
			$('#categorylist option').remove();
	        $.each(data, function (){ 
	        {
	        
	            optionHTML += '<option>'+data[index].category_name+'</option>';
	            index++;
	        
	    	}
	        });
	        $('#categorylist').append('<option selected disabled>Choose a program</option>');
	        $('#categorylist').append(optionHTML);
	        
	        
		},
		error:function()
			{
			alert("error ajax in getCategory() welcomescript.js");
			}
	});
	return false;
};



$("#feedsubmitbtn").click(function(){
	$.ajax({
		method:'POST',
		url:'FeedbackSubmittedServlet',
		dataType:'json',
		data:{
			"name":$("#feedname").val(),
			"feed":$("#feedback").val(),
			"question1_id":window.feedbackquestion1id,
			"question2_id":window.feedbackquestion2id,
			"question3_id":window.feedbackquestion3id,
			"rating1":$("#rating1").val(),
			"rating2":$("#rating2").val(),
			"rating3":$("#rating3").val(),
			"program_id":window.feedbackprogramid
			
		},
		success:function(data)
		{
				$("#feedname").val('');
				$("#feedback").val('');
				$("#feedbackprogramlist").val(1);
				$("#question1").val('');
				$("#question2").val('');
				$("#question3").val('');
				
				$("#rating1").val(5);
				$("#rating2").val(5);
				$("#rating3").val(5);
				
				$('#feedbackmodal').hide();
				$('.modal-backdrop').hide();	
				
		},
		error:function()
			{
			alert("error ajax in feedbacksubmitbtn");
			$("#feedbackmodal").modal("hide");
			$('.modal-backdrop').hide();
			}
		
	});
	return false;
	
});

$("#suggestionsubmitbtn").click(function(){
	$.ajax({
		method:'POST',
		url:'SuggestionSubmissionServlet',
		dataType:'json',
		data:{
			"name":$("#suggestername").val(),
			"suggestion":$("#suggestion").val(),
			"category_name":$("#categorylist").val()
		},
		success:function(data)
		{
				$("#suggestername").val('');
				$("#suggestion").val('');
				$("#categorylist").val('');
				
				$('#suggestionmodal').hide();
				$('.modal-backdrop').hide();	
				
		},
		error:function()
			{
			alert("error ajax in feedbacksubmitbtn");
			$("#suggestionmodal").modal("hide");
			$('.modal-backdrop').hide();
			}
		
	});
	return false;
	
});
