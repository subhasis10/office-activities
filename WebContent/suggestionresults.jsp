<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.enparadigm.activities.connection.DatabaseConnection"%>
<%@page import="com.enparadigm.activities.daos.SuggestionDAO"%>
<%@page import="com.enparadigm.activities.beans.Suggestion"%>
<%@page import="com.enparadigm.activities.daos.CategoryDAO"%>
<%@page import="com.enparadigm.activities.beans.Category"%>
<%@page import="com.enparadigm.activities.daos.Category_countDAO"%>
<%@page import="com.enparadigm.activities.beans.Category_count"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Suggestion Results</title>
	<style>
		.header
		{
			text-align: center;
		}
		.chart-div
		{
			width: fit-content;
			margin: 0 auto;
		}
		#suggestion-table
		{
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			border-collapse: collapse;
			width: 75%;
			margin: 0 auto;
		}
		#suggestion-table td, #suggestion-table th 
		{
  			border: 1px solid #ddd;
  			padding: 8px;
		}

		#suggestion-table tr:nth-child(even){background-color: #f2f2f2;}
		
		#suggestion-table tr:hover {background-color: #ddd;}
		
		#suggestion-table th 
		{
		  padding-top: 12px;
		  padding-bottom: 12px;
		  text-align: left;
		  background-color: #4CAF50;
		  color: white;
		}
		
	</style>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      google.charts.load('current', {'packages':['corechart']});

      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

		      // Create the data table.
		      var data = new google.visualization.DataTable();
		      data.addColumn('string', 'Category');
		      data.addColumn('number', 'Count');
		      
		      	<%
		      	Category_countDAO objCategory_countDAO = new Category_countDAO();
		      	ArrayList<Category_count> category_countList = objCategory_countDAO.fetchCategory_countList();
				for(Category_count category_count : category_countList)
				{
				%>
					data.addRow(['<%= category_count.getCategory_name() %>', <%= category_count.getSuggestion_count() %>]);
				<%
				}
	      		%>
		
		      // Set chart options
		      var options = {'title':'How many responses each category got',
		                     'width': 600,
		                     'height': 400};
		
		      // Instantiate and draw our chart, passing in some options.
		      var chart = new google.visualization.PieChart(document.getElementsByClassName('chart-div')[0]);
		      chart.draw(data, options);
    }
     </script>
</head>
<body>
	<div class="header"> <h1> Suggestion Results </h1> </div>
	<div class="chart-div"></div>
	<div class="stable">
	<%
		SuggestionDAO objSuggestionDAO = new SuggestionDAO();
		ArrayList<Suggestion> SuggestionList = objSuggestionDAO.fetchSuggestionList();
		CategoryDAO objCategoryDAO = new CategoryDAO();
	%>
		<table id="suggestion-table">
			<tr>
    			<th>Serial No</th>
    			<th>Suggester Name</th>
    			<th>Suggestion</th>
    			<th>Category</th>
    			<th>Submitted At</th>
  			</tr>
			<%
				int i = 1;
				for(Suggestion suggestion : SuggestionList)
				{
			%>
					<tr>
					<td><%= i++ %></td>
					<td><%= suggestion.getSuggester_name() %></td>
					<td><%= suggestion.getSuggestion() %></td>
					<td><%= objCategoryDAO.fetchCategoryById(suggestion.getCategory_id()).getCategory_name() %></td>
					<td><%= suggestion.getCreated_at() %></td>
					</tr>
			<%
				}
			%>
		</table>
	</div>
	
</body>
</html>