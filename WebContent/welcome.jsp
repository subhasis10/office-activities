<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="#"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/design.css" >

<title>Office Activity</title>
</head>
<body>
<div class="col">
<!--Heading Section-->
<div class="row">
<h1>Office Activity</h1>
<br>
<hr>
<br>
</div>
<!--Action Buttons-->
<div class="actionbutton" class="row">

<div class="col-xs-5"></div>
<div class="col-xs-4">
<button class="homepagebutton" name="addsuggestionbtn" id="addsuggestionbtn" class="btn btn-success"  data-toggle="modal" data-target="#suggestionmodal">Suggestion</button>
&nbsp;
<button class="homepagebutton" name="addfeedbackbtn" id="addfeedbackbtn" class="btn btn-success"  data-toggle="modal" data-target="#feedbackmodal">Feedback</button>
&nbsp;
<button class="homepagebutton" type="button" class="btn btn-success"  name="adminloginbtn" id="adminloginbtn" data-toggle="modal" data-target="#loginmodal">Login</button>
&nbsp;
</div>
<div class="col-xs-5"></div>
<br>
<br>
<!--Upcoming Program List Section-->
<div class="panel-body" class="row">
<h3>Upcoming Programs</h3>
<input type="hidden" id="currentDate"></input>
<div class="col-xs-3"></div>
<div class="col-xs-6">
<table id="upcomingprogramlist-table" class="table table-bordered" >
			
			<thead>
			<tr>
			<th style="text-align:center"> Program Name</th>
			<th style="text-align:center">Date</th>
			</tr>
			</thead>
			<tbody></tbody>
</table>
</div>
<div class="col-xs-3"></div>	
</div>
</div>
</div>
<!-- Login Modal division -->
<div class="modal fade" id="loginmodal" >
			  <div class="modal-dialog">
    			<div class="modal-content" >
    			  	<div class="modal-header">
        			<h3 class="modal-title" >Login</h3>
        			<button type="button" class="close" data-dismiss="modal" >
          			<span>&times;</span>
        			</button>
      				</div>
      				
      				
      				<div class="modal-body">
        
								
					<div  style="text-align: center;" class="form-group" class="row">
					<label>Name</label>
					<input type="text" name="name" id="name" class="form-group" >
					</div>
	
					<div  style="text-align: center;" class="form-group" class="row">
					<label>Password</label>
					<input type="password" name="password"id="password" class="form-group" >
					</div>
			

    				</div>
      				<div class="modal-footer">
        
       				 <div  style="text-align: center;"class="form-group" class="row">
					<button type="button"  class="btn btn-primary" id="loginbtn" name="loginbtn">Login</button>
					<button type="button" class="btn btn-secondary"  id="closebtn" class="close" data-dismiss="modal">Close</button>
					</div>
        			</div>
        		</div>
      		</div>
 </div>
 <!-- Feedback Modal -->
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="feedbackmodal" >
			  <div class="modal-dialog modal-lg">
    			<div class="modal-content" >
    			  	<div class="modal-header">
        			<h3 class="modal-title" >Feedback</h3>
        			<button type="button" class="close" data-dismiss="modal" >
          			<span>&times;</span>
        			</button>
      				</div>
      				
      				
      				<div class="modal-body">
        			<div  style="text-align: center;" class="form-group" class="row">
					<label>Name</label>
					<input type="text" name="feedname" id="feedname" placeholder="Not Mandatory" class="form-group" >
					</div>
					<div class="form-group" class="row">
  					<label >Program</label>
  					<select class="form-control" id="feedbackprogramlist" onchange="getFeedbackQuestions(this)">
  					<option></option>
  					</select>
					</div>
					<div class="row">
						<div class="col-xs-8">
							<div style="text-align: center;" class="form-group" >
							<label>Feedback</label>
							<textarea class="form-control" rows="15" id="feedback"></textarea>
							</div>
						</div>
						<div class="col-xs-4">
							<br>
							<div id="feedbackquestionlist">
							<div></div>
							</div>
						</div>
						
						
				</div>
				</div>
      			<div class="modal-footer">
        
       				 <div  style="text-align: center;"class="form-group">
					<button type="button"  class="btn btn-primary" id="feedsubmitbtn" name="feedsubmitbtn">Submit</button>
					<button type="button" class="btn btn-secondary"  id="closebtn" class="close" data-dismiss="modal">Close</button>
					</div>
        </div>
        		</div>
</div>
 </div>
 <!-- Suggestion Modal -->
 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="suggestionmodal" >
			  <div class="modal-dialog modal-lg">
    			<div class="modal-content" >
    			  	<div class="modal-header">
        			<h3 class="modal-title" >Suggestion</h3>
        			<button type="button" class="close" data-dismiss="modal" >
          			<span>&times;</span>
        			</button>
      				</div>
      				
      				
      				<div class="modal-body">
        			<div  style="text-align: center;" class="form-group" class="row">
					<label>Name</label>
					<input type="text" name="suggestername" id="suggestername" placeholder="Not Mandatory" class="form-group" >
					</div>
					<div class="form-group" class="row">
  					<label >Category</label>
  					<select class="form-control" id="categorylist" onchange="getSuggestion(this)">
  					<option></option>
  					</select>
					</div>
					<div class="row">
						
							<div style="text-align: center;" class="form-group" >
							<label>Suggestion</label>
							<textarea class="form-control" rows="10" id="suggestion"></textarea>
							</div>
						
						
						
				</div>
				</div>
      			<div class="modal-footer">
        
       				 <div  style="text-align: center;"class="form-group">
					<button type="button"  class="btn btn-primary" id="suggestionsubmitbtn" name="suggestionsubmitbtn">Submit</button>
					<button type="button" class="btn btn-secondary"  id="closebtn" class="close" data-dismiss="modal">Close</button>
					</div>
        </div>
        		</div>

 </div>
 
 </div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/welcomescript.js"></script>
</body>
</html>