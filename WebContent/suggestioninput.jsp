<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.enparadigm.activities.daos.CategoryDAO"%>
<%@page import="com.enparadigm.activities.beans.Category"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Suggestion Input</title>
<style>

h2
{
	text-align: center;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

textarea
{
	height: 300px;
}
input[type=submit] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.suggestion-div {
	width: 50%;
	border-radius: 5px;
	background-color: #f2f2f2;
	padding: 20px 20px 50px;
	margin: 0 auto;
}
</style>
</head>
<body>

	<h2>Give Suggestion</h2>

	<div class = "suggestion-div">
	  <form action="controller/insertsuggestion.jsp" autocomplete="off">
	    <label for="suggester_name">Name</label>
	    <input type="text" id="suggester_name" name="suggester_name" placeholder="Name">
	
	    <label for="category_name">Category</label>
	    <select id="category_name" name="category_name">
	    <%
	    	CategoryDAO categoryDAO = new CategoryDAO();
    		ArrayList<Category> categoryList = categoryDAO.fetchCategoryList();
    		for(Category category : categoryList)
    		{
    	%>
				<option value=<%= category.getCategory_id() %>> <%= category.getCategory_name() %> </option>
	    <%
    		}
	    %>
	    </select>
	    
	  	<label for="suggestion">Suggestion</label>
	  	<textarea id="suggestion" name="suggestion" placeholder="Write something.."></textarea>
	  	<div style = "width: 100%">
			<input type="submit" value="Submit">
		</div>
	  </form>
	</div>

</body>
</html>