<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.enparadigm.activities.daos.SuggestionDAO"%>
<%@page import="com.enparadigm.activities.beans.Suggestion"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script>
	function myFunction() 
	{
	  alert("Suggestion Submitted Successfully");
	}
</script>
</head>
<body>
<%
	String suggester_name = request.getParameter("suggester_name");
	String category_name = request.getParameter("category_name");
	int category_id = Integer.parseInt(category_name);
	String suggestion = request.getParameter("suggestion");
	
	Suggestion objSuggestion = new Suggestion();
	objSuggestion.setSuggester_name(suggester_name);
	objSuggestion.setSuggestion(suggestion);
	objSuggestion.setCategory_id(category_id);
	SuggestionDAO objSuggestionDAO = new SuggestionDAO();
	boolean check = false;
	check = objSuggestionDAO.insertSuggestion(objSuggestion);
	if(check == true)
	{
%>
		<script>
			myFunction();
			window.location.href="http://localhost:8080/activities/suggestioninput.jsp"
		</script>
<%
	}
%>
	
	
</body>
</html>